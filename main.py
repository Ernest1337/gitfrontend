#!/usr/bin/python

import http.server
import socketserver
import os
import sys
import shutil
import git
import platform

System = platform.system()
PORT = 8080
Handler = http.server.SimpleHTTPRequestHandler
CWD = os.getcwd()
staticFilesDirName = "build"
pathToStaticFiles = os.path.join(CWD, staticFilesDirName)

def clean():
    shutil.rmtree("build/")

def printError():
    sys.stderr.write("That's not quite right!\n")

def printHelp():
    print("Possible parameters:")
    print("-h        display this help screen.")
    print("--help    display this help screen.")
    print("-gs       generate static files and serve them on a localhost.")
    print("--genserv generate static files and serve them on a localhost.")
    print("-g [path to repo]    generate static files.")
    print("--gen     generate static files.")
    print("-s        serve generated files on a localhost.")
    print("--serve   serve generated files on a localhost.")
    print("-c        clean after previous build.")
    print("--clean   clean after previous build.")

def runServer():
    global Handler
    global pathToStaticFiles
    os.chdir(pathToStaticFiles)
    with socketserver.TCPServer(("", PORT), Handler) as httpd:
        print("serving at port:", PORT)
        print("URL: http://localhost:"+str(PORT)+"/")
        httpd.serve_forever()

def generate(pathToRepo):
    global pathToStaticFiles
    global CWD
    global System
    global staticFilesDirName
    if System=="Linux":
        if str(pathToRepo)[0]=="/":
            pass
        else:
            pathToRepo = CWD+"/"+pathToRepo
    elif System=="Windows":
        if str(pathToRepo)==".":
            pass
        elif str(pathToRepo)[2]=="\\":
            pass
        else:
            pathToRepo = CWD+"\\"+pathToRepo
    Repo = git.Repo(pathToRepo)
    AllCommitsList = list(Repo.iter_commits('master'))
    AllCommitsList.reverse()
    NumberOfPagesWithCommits = int(len(AllCommitsList)/50)
    NumberOfCommitsOnLastPage = len(AllCommitsList)-(50*NumberOfPagesWithCommits)

    """
    for a in range(NumberOfPagesWithCommits): # generate pages with commits
        for i in range(50):  # generate page with first 50 commits
            print()
            print("Commit nmbr."+str(i+1+(50*a)))
            print(AllCommitsList[i+(50*a)])
            print(AllCommitsList[i+(50*a)].author)
            print(AllCommitsList[i+(50*a)].message)
    for i in range(NumberOfCommitsOnLastPage):
        print()
        print("Commit nmbr."+str(i+1+(NumberOfPagesWithCommits*50)))
        print(AllCommitsList[i+(NumberOfPagesWithCommits*50)])
        print(AllCommitsList[i+(NumberOfPagesWithCommits*50)].author)
        print(AllCommitsList[i+(NumberOfPagesWithCommits*50)].message)
    """
    shutil.copytree("src/css",staticFilesDirName+"/css")
    shutil.copytree("src/assets",staticFilesDirName+"/assets")
    with open("src/index.html","r") as finalFile:
        with open(staticFilesDirName+"/index.html","w") as outputIndex:
            # finalFile = finalFile.read()
            var1 = 0
            templateLinesList = []
            for i in finalFile:
                templateLinesList.append(i)
                var1 += 1
                if "<!--PY_README-->" in i:
                    lineToReplace = var1
                    print("line:", lineToReplace)
                    print(templateLinesList[var1-1])
            var1 = 0
            with open("src/index.html","r") as finalFile:
                for i in finalFile:
                    var1 += 1
                    if var1==lineToReplace:
                        outputIndex.write("test!!!!!!\n")
                    else:
                        outputIndex.write(i)
        return 0
        for a in range(NumberOfPagesWithCommits): # generate pages with commits
            for i in range(50):  # generate page with first 50 commits
                print()
                print("Commit nmbr."+str(i+1+(50*a)))
                print(AllCommitsList[i+(50*a)])
                print(AllCommitsList[i+(50*a)].author)
                print(AllCommitsList[i+(50*a)].message)
        for i in range(NumberOfCommitsOnLastPage):
            print()
            print("Commit nmbr."+str(i+1+(NumberOfPagesWithCommits*50)))
            print(AllCommitsList[i+(NumberOfPagesWithCommits*50)])
            print(AllCommitsList[i+(NumberOfPagesWithCommits*50)].author)
            print(AllCommitsList[i+(NumberOfPagesWithCommits*50)].message)

def createDir():
    global CWD
    global staticFilesDirName
    try: # NEED TO REMOVE THIS TRY EXCEPT LATER
        os.mkdir(os.path.join(CWD, staticFilesDirName))
    except:
        pass

def main():
    if len(sys.argv)==1:
        printError()
        printHelp()
    elif len(sys.argv)==2:
        if sys.argv[1]=="-h" or sys.argv[1]=="--help":
            printHelp()
        elif sys.argv[1]=="-c" or sys.argv[1]=="--clean":
            clean()
        elif sys.argv[1]=="-gs" or sys.argv[1]=="--genserv":
            try:
                createDir()
                pathToRepo = sys.argv[2]
                generate(pathToRepo)
                runServer()
            except OSError:
                inp = input("Target directory exist, do you want to run the server anyway? (y/n): ")
                if inp=="y" or inp=="":
                    runServer()
                else:
                    quit()
        elif sys.argv[1]=="-s" or sys.argv[1]=="--serve":
            try:
                runServer()
            except:
                printError()
                quit()
    elif len(sys.argv)==3:
        if sys.argv[1]=="-g" or sys.argv[1]=="--gen":
                #try:
            createDir()
            pathToRepo = sys.argv[2]
            generate(pathToRepo)
                #except:
                    #print("Target directory exist!")
                    #quit()

if __name__ == "__main__":
    main()
